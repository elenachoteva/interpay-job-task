## Technical description (legacy development purposes)

The project has two pages - a main page and a search-result page. The main page is where the complete listing of works is composed by using a parsing logic that is fired upon loading the page. 
The search-result page shows result of user's search by author name through the records in the database.

## Commercial use description (end user purposes)

Here's a virtual library where you can browse for your favorite piece of literature. You can go through the listing in our main page or use the search functionality to search by author.

## Run project

 - git clone https://elenachoteva@bitbucket.org/elenachoteva/interpay-job-task.git
 - cp .env.example .env
 - fill database connection parameters in .env file
 - composer install
 - php artisan key:generate
 - php artisan migrate
 - php artisan storage:link
 - npm install
 - npm run production
 - php artisan serve
 - visit http://127.0.0.1:8000 

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
