<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'author',
        'genre',
        'description',
        'filepath'
    ];

    /**
     * Indicates if the model should be timestamped
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * @var array
     */
    public static $storeRules = [
        'name' => 'required|string|max:200',
        'author' => 'required|string|max:120',
        'genre' => 'string|max:50|nullable',
        'description' => 'string|nullable'
    ];

    /**
     * @var array
     */
    public static $updateRules = [
        'name' => 'required|string|max:200',
        'author' => 'required|string|max:120',
        'genre' => 'string|max:50|nullable',
        'description' => 'string|nullable'
    ];

    /**
     * @param $search
     * @return mixed
     */
    public static function searchInBooks($search)
    {
        return (new static)::where('author', 'ilike', '%' . $search . '%')
            ->orderBy('updated_at', 'DESC')
            ->get();
    }
}
