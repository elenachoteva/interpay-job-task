<?php

namespace App\Http\Controllers;

use App\Book;
use App\Services\XmlService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application homepage.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $xmlObject = new XmlService(storage_path('app/public/books'));
        $result = $xmlObject->directoryIterator();

        return view('index', compact('result'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function search(Request $request)
    {
        if ($request->has('search') && $request->filled('search')) {
            return redirect()->route('searchResult', [
                'search' => $request->get('search')
            ]);
        }

        return redirect()->route('searchResult')->with('message', 'No books found');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchResult(Request $request)
    {
        $books = Book::searchInBooks($request->get('search'));

        return view('search_result', compact('books'))
            ->with('message', 'No books found')
            ->with('search', $request->get('search'));
    }
}
