<?php

namespace App\Services;

use App\Book;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class XmlService
{
    private $path;

    /**
     * XmlService constructor.
     * @param $path
     */
    public function __construct($path)
    {
        $this->path = $path;
    }

    /**
     * @return array
     */
    public function directoryIterator()
    {
        $booksList = [];

        $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($this->path));
        $allXmlFiles = array_filter(iterator_to_array($iterator), function ($file) {
            return $file->isFile() && $file->getExtension() === 'xml';
        });

        if (count($allXmlFiles) > 0) {
            foreach ($allXmlFiles as $key => $file) {
                $booksList[] = $this->readFile($key);
            }
        }

        return $booksList;
    }

    /**
     * @param $filepath
     * @return array|string
     */
    public function readFile($filepath)
    {
        $books = [];

        $xmlObject = simplexml_load_file($filepath, 'SimpleXMLElement');

        if ($xmlObject === false) {
            \Log::error(['Xml books - error parsing document']);
            return 'Error';
        } else {
            if (!$xmlObject->book) {
                return 'Error';
            }

            foreach ($xmlObject->book as $item) {
                $book = Book::updateOrCreate(
                    [
                        'author' => (string)$item->author,
                        'name' => (string)$item->name,
                        'filepath' => (string)$filepath
                    ],
                    [
                        'genre' => (string)$item->genre,
                        'description' => (string)$item->description,
                        'author' => (string)$item->author,
                        'name' => (string)$item->name,
                        'filepath' => (string)$filepath
                    ]
                );

                $books[] = $book;
            }
        }

        return $books;
    }
}
