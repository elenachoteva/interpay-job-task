@extends('layouts.app')

@section('content')
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Author</th>
            <th scope="col">Genre</th>
            <th scope="col">Description</th>
            <th scope="col">Filepath</th>
            <th scope="col">Created</th>
            <th scope="col">Last Updated</th>
        </tr>
        </thead>
        <tbody>
        @forelse($result as $item)
            @forelse($item as $book)
                <tr>
                    <td>{{ $book->name }}</td>
                    <td>{{ $book->author }}</td>
                    <td>{{ $book->genre }}</td>
                    <td>{{ $book->description }}</td>
                    <td>{{ $book->filepath }}</td>
                    <td>{{ $book->created_at->format('d.m.Y H:i') }}</td>
                    <td>{{ $book->updated_at->format('d.m.Y H:i') }}</td>
                </tr>
            @empty
            @endforelse
        @empty
            <tr>
                <td colspan="7">
                    <div class="alert alert-primary" role="alert">
                        No books found.
                    </div>
                </td>
            </tr>
        @endforelse
        </tbody>
    </table>
@endsection
